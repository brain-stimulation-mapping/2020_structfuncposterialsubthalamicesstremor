# 2020_StructFuncPosterialSubthalamicEssTremor

Levy et al. (2020), Structure-function relationship of the posterior subthalamic area with directional deep brain stimulation for essential tremor, https://doi.org/10.1016/j.nicl.2020.102486 .

This repo has the analysis scripts only but not the Nifti files with the volumes of tissue activated. Please contact the main author, if you would like to have this. The analysis scripts are the final version used for the paper and the repo does not include the earlier revisions.
