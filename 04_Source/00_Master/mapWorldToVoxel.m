function voxelCoordinates = mapWorldToVoxel(worldCoordinates,image)

worldCoordinates = [worldCoordinates';ones(1,size(worldCoordinates,1))];
voxelCoordinates = image.mat\worldCoordinates;
voxelCoordinates = round(voxelCoordinates);
voxelCoordinates(voxelCoordinates<1) = 1;
voxelCoordinates = voxelCoordinates(1:3,:)';
end
