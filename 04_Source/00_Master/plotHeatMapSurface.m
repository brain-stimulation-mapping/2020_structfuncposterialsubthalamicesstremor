function surfaceHandle = plotHeatMapSurface(heatMap, figureHandle, hemisphere, threshold, figureTitle, surfaceColor)

tmpIdx = find(heatMap.img > threshold);
if ~isempty(tmpIdx)
    [xx,yy,zz] = ind2sub(size(heatMap.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,heatMap);
    boundaryCluster = boundary(heatMapWorldCoordinates,0.25);
    
    figure(figureHandle)
    surfaceHandle = trisurf(boundaryCluster, heatMapWorldCoordinates(:,1), ...
        heatMapWorldCoordinates(:,2), heatMapWorldCoordinates(:,3),...
        'FaceColor',surfaceColor,'FaceAlpha',.8,'LineStyle','none');
    
    if strcmp(hemisphere,'left')
        view(-30,30)
    elseif strcmp(hemisphere,'right')
        view(30,30)
    else
        view(0,30)
    end
    cb = colorbar;
    if max(heatMap.img(:)) > threshold+1
        caxis([threshold+1 max(heatMap.img(:))])
    else
        caxis([threshold threshold+1])
    end
    cb.Label.String = 'Number of activation';
end
title(figureTitle)

end
