% Essential tremor heat maps, directionality analysis
% After several discussions, prepare figures for manuscript
% Rev03, January 2020, T. A. Khoa Nguyen

clc
clearvars; close all

mapSize = 100; % container for aggregated VTA

%% Effect heat map
threshold = 1;
contactsLeft = {'C1','C2','C3','C4','C5','C6','C7','C8','C234','C567'};
contactsRight = {'C9','C10','C11','C12','C13','C14','C15','C16','C101112','C131415'};
load('therapeuticWindowsRev03.mat') % loading therapeutic windows, thresholdTable
doPrint = false;

baseFolder = ['..\03_Data\mappingETRev03\'];
patientFolders = dir(baseFolder);
patientFolders(1:2) = []; % delete . and ..
sideEffectCategories = dir([baseFolder filesep patientFolders(1).name filesep 'sideEffect\']);
sideEffectCategories(1:2) = [];

% load Lead-DBS 2.2.3
% reslice_nii('LeadDBS2x\Code\testing\templates\space\MNI_ICBM_2009b_NLIN_ASYM\t2.nii',...
% 'anatT2Resliced.nii',[0.25 0.25 0.25],false)
aggregatedVTAs = ea_load_nii('anatT2Resliced.nii');
aggregatedVTAs.img = zeros(mapSize, mapSize, mapSize);
aggregatedVTAs.dim = [mapSize, mapSize, mapSize];
aggregatedVTAs.mat(:,4) = [0; -25; -20; 1]; % set origin for DRTT
counterReducedEffectVTA = 0;
counterFullEffectVTA = 0;

for patientIndex = 1:length(patientFolders)
    effectFolders = dir([baseFolder filesep...
        patientFolders(patientIndex).name filesep 'effect\']);
    effectFolders(1:2) = [];

    for stimulationIndex = 1:length(effectFolders)
        fileNameVTA = [effectFolders(1).folder filesep...
            effectFolders(stimulationIndex).name filesep 'vat_left.nii'];
        hemisphere = 'left';
        if exist(fileNameVTA,'file') ~= 2
            fileNameVTA = [effectFolders(1).folder filesep ...
                effectFolders(stimulationIndex).name filesep 'vat_right.nii'];
            hemisphere = 'right';
        end
        if exist(fileNameVTA,'file') ~= 2
            fprintf('%s %s VTA does not exist. Skipped.\n',...
                patientFolders(patientIndex).name, ...
                effectFolders(stimulationIndex).name);
            continue
        end
        tempContact = (strsplit(effectFolders(stimulationIndex).name,'C'));
        effectContact = ['C' tempContact{end}];

        %         if ((thresholdTable{patientIndex,effectContact}(1)) < ... % test with strictly less or less or equal
        %             (thresholdTable{patientIndex,effectContact}(2:5)))
        %             disp('load effect VTA normally')
        % add effect VTA
        reslice_nii(fileNameVTA,...
            'tmpVTAResliced.nii',aggregatedVTAs.voxsize,false)
        VTANifti = ea_load_nii('tmpVTAResliced.nii');
        VTANifti.img(isnan(VTANifti.img)) = 0;
        VTANifti.img = round(VTANifti.img); % clean up
        % 'find' returns linear index, need to convert to subscript
        [xx,yy,zz] = ind2sub(size(VTANifti.img),find(VTANifti.img>0));
        if ~isempty(xx)
            VTAVoxelCoordinates = [xx,yy,zz];
            % map from voxel coordinates to millimeter in MNI world coordinates
            VTAWorldCoordinates = mapVoxelToWorld(VTAVoxelCoordinates,VTANifti);
            if strcmp(hemisphere,'left')
                VTAWorldCoordinates = ea_flip_lr_nonlinear(VTAWorldCoordinates);
            end
            % map from millimeter world coordinates to voxel coordinates of container
            aggregatedVTAsVoxelCoordinates = mapWorldToVoxel(VTAWorldCoordinates,aggregatedVTAs);
            linearIdx = sub2ind(size(aggregatedVTAs.img),... % get linear index
                aggregatedVTAsVoxelCoordinates(:,1),...
                aggregatedVTAsVoxelCoordinates(:,2),...
                aggregatedVTAsVoxelCoordinates(:,3));
            aggregatedVTAs.img(unique(linearIdx)) = ...
                aggregatedVTAs.img(unique(linearIdx)) + 1;
        end
        %         else
        if ((thresholdTable{patientIndex,effectContact}(1)) > ... % negative therapeutic window
                min(thresholdTable{patientIndex,effectContact}(2:end)))
            fprintf('%s %s Subtracting side effect VTA from effect VTA.\n',...
                patientFolders(patientIndex).name,...
                effectFolders(stimulationIndex).name);
            [~,sideEffectIndex] = min(thresholdTable{patientIndex,effectContact}(2:end));
            if sideEffectIndex == 5 % Xtra side effect
                tmpDirectory = dir([baseFolder filesep ...
                    patientFolders(patientIndex).name filesep ...
                    'sideEffect' filesep ...
                    sideEffectCategories(sideEffectIndex).name filesep]);
                tmpDirectory(1:2) = [];
                % looking for the VTA folder with the correct contact
                checkFolderNames = strfind({tmpDirectory.name},effectContact);
                emptyCells = cellfun(@isempty,checkFolderNames);
                tmpSideEffectFolder = tmpDirectory(emptyCells==0).name;
            else
                tmpSideEffectFolder = sideEffectCategories(sideEffectIndex).name;
            end
            if ismember(effectContact, contactsRight)
                fileNameSideEffectVTA = [baseFolder filesep ...
                    patientFolders(patientIndex).name filesep ...
                    'sideEffect' filesep ...
                    sideEffectCategories(sideEffectIndex).name filesep ...
                    tmpSideEffectFolder effectContact ...
                    filesep 'vat_right.nii'];
                hemisphere = 'right';
            elseif ismember(effectContact, contactsLeft)
                fileNameSideEffectVTA = [baseFolder filesep ...
                    patientFolders(patientIndex).name filesep ...
                    'sideEffect' filesep ...
                    sideEffectCategories(sideEffectIndex).name filesep ...
                    tmpSideEffectFolder effectContact ...
                    filesep 'vat_left.nii'];
                hemisphere = 'left';
            end
            if exist(fileNameSideEffectVTA,'file') ~= 2
                fprintf('%s %s VTA does not exist. Skipped.\n',...
                    patientFolders(patientIndex).name, sideEffectCategories(sideEffectIndex).name);
                continue
            end
            reslice_nii(fileNameSideEffectVTA,...
                'tmpVTAResliced.nii',aggregatedVTAs.voxsize,false)
            VTANifti = ea_load_nii('tmpVTAResliced.nii');
            VTANifti.img(isnan(VTANifti.img)) = 0;
            VTANifti.img = round(VTANifti.img);
            % 'find' returns linear index, need to convert to subscript
            [xx,yy,zz] = ind2sub(size(VTANifti.img),find(VTANifti.img>0));
            if ~isempty(xx)
                VTAVoxelCoordinates = [xx,yy,zz];
                % map from voxel coordinates to millimeter in MNI world coordinates
                VTAWorldCoordinates = mapVoxelToWorld(VTAVoxelCoordinates,VTANifti);
                if strcmp(hemisphere,'left')
                    VTAWorldCoordinates = ea_flip_lr_nonlinear(VTAWorldCoordinates);
                end
                % map from millimeter world coordinates to voxel coordinates of container
                aggregatedVTAsVoxelCoordinates = mapWorldToVoxel(VTAWorldCoordinates,aggregatedVTAs);
                linearIdx = sub2ind(size(aggregatedVTAs.img),... % get linear index
                    aggregatedVTAsVoxelCoordinates(:,1),...
                    aggregatedVTAsVoxelCoordinates(:,2),...
                    aggregatedVTAsVoxelCoordinates(:,3));
                aggregatedVTAs.img(unique(linearIdx)) = ...
                    aggregatedVTAs.img(unique(linearIdx)) - 1;
            end
            counterReducedEffectVTA = counterReducedEffectVTA + 1;
        else
            counterFullEffectVTA = counterFullEffectVTA + 1;
        end
    end
end

delete tmpVTAResliced.nii

fprintf('%d full effect VTAs, %d reduced effect VTAs included.\n',...
    counterFullEffectVTA, counterReducedEffectVTA)

[fhtmp, fhMappingRight] = setupHeatMap();
close(fhtmp)
heatMapTremorReductionPooled = aggregatedVTAs;
plotHeatMap(heatMapTremorReductionPooled, fhMappingRight, 'right', 1, 'Tremor reduction pooled',2);

%% for every side effect, pool VTAs by i) subtracting effect VTA, ii) or adding full VTA
baseFolder = ['..03_Data\mappingETRev03\'];

aggregatedSideEffectVTAs = ea_load_nii(...
    'anatT2Resliced.nii');
aggregatedSideEffectVTAs.img = zeros(mapSize, mapSize, mapSize);
aggregatedSideEffectVTAs.dim = [mapSize, mapSize, mapSize];
aggregatedSideEffectVTAs.mat(:,4) = [0; -25; -20; 1]; % set origin for DRTT

for sideEffectIndex = 1:length(sideEffectCategories)-1 % don't do Xtra
    % dysarthria, dystonia, paresthesia, spasm
    % clear heat map, reset counters
    fprintf('Parsing %s.\n',sideEffectCategories(sideEffectIndex).name);
    aggregatedSideEffectVTAs.img = zeros(mapSize, mapSize, mapSize);
    counterSideEffectMinusEffectVTA = 0;
    counterSideEffectVTAFull = 0;
    for patientIndex = 1:length(patientFolders)
        stimFolders = dir([baseFolder filesep ...
            patientFolders(patientIndex).name filesep 'sideEffect' filesep ...
            sideEffectCategories(sideEffectIndex).name filesep]);
        stimFolders(1:2) = [];

        if ~isempty(stimFolders)
            fprintf('Patient %s has %s.\n', patientFolders(patientIndex).name,...
                sideEffectCategories(sideEffectIndex).name);
            for tmpIndex = 1:length(stimFolders)
                tempContact = (strsplit(stimFolders(tmpIndex).name,'C'));
                sideEffectContact = ['C' tempContact{end}];
                if ismember(sideEffectContact, contactsRight)
                    fileNameSideEffectVTA = [stimFolders(1).folder filesep ...
                        stimFolders(tmpIndex).name filesep 'vat_right.nii'];
                    fileNameVTA = [baseFolder filesep ...
                        patientFolders(patientIndex).name filesep 'effect' ...
                        filesep 'DRTTRight' sideEffectContact filesep 'vat_right.nii'];
                    hemisphere = 'right';
                elseif ismember(sideEffectContact, contactsLeft)
                    fileNameSideEffectVTA = [stimFolders(1).folder filesep ...
                        stimFolders(tmpIndex).name filesep 'vat_left.nii'];
                    fileNameVTA = [baseFolder filesep ...
                        patientFolders(patientIndex).name filesep 'effect' ...
                        filesep 'DRTTLeft' sideEffectContact filesep 'vat_left.nii'];
                    hemisphere = 'left';
                end
                if exist(fileNameSideEffectVTA,'file') ~= 2
                    fprintf('%s %s VTA does not exist. Skipped.\n',...
                        patientFolders(patientIndex).name, stimFolders(tmpIndex).name);
                    continue
                end
                if exist(fileNameVTA,'file') ~= 2
                    fprintf('%s %s effect VTA does not exist. Skipped.\n',...
                        patientFolders(patientIndex).name, stimFolders(tmpIndex).name);
                    continue
                end

                if ((thresholdTable{patientIndex,sideEffectContact}(1)) < ...
                        (thresholdTable{patientIndex,sideEffectContact}(sideEffectIndex+1)))
                    fprintf('%s Subtract effect VTA from side effect VTA.\n', ...
                        stimFolders(tmpIndex).name);
                    reslice_nii(fileNameSideEffectVTA,...
                        'tmpVTAResliced.nii',aggregatedVTAs.voxsize,false)
                    VTANifti = ea_load_nii('tmpVTAResliced.nii');
                    VTANifti.img(isnan(VTANifti.img)) = 0;
                    VTANifti.img = round(VTANifti.img);
                    % 'find' returns linear index, need to convert to subscript
                    [xx,yy,zz] = ind2sub(size(VTANifti.img),find(VTANifti.img>0));
                    if ~isempty(xx)
                        VTAVoxelCoordinates = [xx,yy,zz];
                        % map from voxel coordinates to millimeter in MNI world coordinates
                        VTAWorldCoordinates = mapVoxelToWorld(VTAVoxelCoordinates,VTANifti);
                        if strcmp(hemisphere,'left')
                            VTAWorldCoordinates = ea_flip_lr_nonlinear(VTAWorldCoordinates);
                        end
                        % map from millimeter world coordinates to voxel coordinates of container
                        aggregatedVTAsVoxelCoordinates = mapWorldToVoxel(VTAWorldCoordinates,aggregatedSideEffectVTAs);
                        linearIdx = sub2ind(size(aggregatedSideEffectVTAs.img),... % get linear index
                            aggregatedVTAsVoxelCoordinates(:,1),...
                            aggregatedVTAsVoxelCoordinates(:,2),...
                            aggregatedVTAsVoxelCoordinates(:,3));
                        aggregatedSideEffectVTAs.img(unique(linearIdx)) = ...
                            aggregatedSideEffectVTAs.img(unique(linearIdx)) + 1;
                    end
                    % load effect VTA and subtract it
                    reslice_nii(fileNameVTA,...
                        'tmpVTAResliced.nii',aggregatedVTAs.voxsize,false)
                    VTANifti = ea_load_nii('tmpVTAResliced.nii');
                    VTANifti.img(isnan(VTANifti.img)) = 0;
                    VTANifti.img = round(VTANifti.img);
                    % 'find' returns linear index, need to convert to subscript
                    [xx,yy,zz] = ind2sub(size(VTANifti.img),find(VTANifti.img>0));
                    if ~isempty(xx)
                        VTAVoxelCoordinates = [xx,yy,zz];
                        % map from voxel coordinates to millimeter in MNI world coordinates
                        VTAWorldCoordinates = mapVoxelToWorld(VTAVoxelCoordinates,VTANifti);
                        if strcmp(hemisphere,'left')
                            VTAWorldCoordinates = ea_flip_lr_nonlinear(VTAWorldCoordinates);
                        end
                        % map from millimeter world coordinates to voxel coordinates of container
                        aggregatedVTAsVoxelCoordinates = mapWorldToVoxel(VTAWorldCoordinates,aggregatedSideEffectVTAs);
                        linearIdx = sub2ind(size(aggregatedSideEffectVTAs.img),... % get linear index
                            aggregatedVTAsVoxelCoordinates(:,1),...
                            aggregatedVTAsVoxelCoordinates(:,2),...
                            aggregatedVTAsVoxelCoordinates(:,3));
                        aggregatedSideEffectVTAs.img(unique(linearIdx)) = ...
                            aggregatedSideEffectVTAs.img(unique(linearIdx)) - 1;
                    end

                    counterSideEffectMinusEffectVTA = ...
                        counterSideEffectMinusEffectVTA + 1;
                else
                    fprintf('%s Add full side effect VTA.\n', ...
                        stimFolders(tmpIndex).name);
                    reslice_nii(fileNameSideEffectVTA,...
                        'tmpVTAResliced.nii',aggregatedVTAs.voxsize,false)
                    VTANifti = ea_load_nii('tmpVTAResliced.nii');
                    VTANifti.img(isnan(VTANifti.img)) = 0;
                    VTANifti.img = round(VTANifti.img); % clean up
                    % 'find' returns linear index, need to convert to subscript
                    [xx,yy,zz] = ind2sub(size(VTANifti.img),find(VTANifti.img>0));
                    if ~isempty(xx)
                        VTAVoxelCoordinates = [xx,yy,zz];
                        % map from voxel coordinates to millimeter in MNI world coordinates
                        VTAWorldCoordinates = mapVoxelToWorld(VTAVoxelCoordinates,VTANifti);
                        if strcmp(hemisphere,'left')
                            VTAWorldCoordinates = ea_flip_lr_nonlinear(VTAWorldCoordinates);
                        end
                        % map from millimeter world coordinates to voxel coordinates of container
                        aggregatedVTAsVoxelCoordinates = mapWorldToVoxel(VTAWorldCoordinates,aggregatedSideEffectVTAs);
                        linearIdx = sub2ind(size(aggregatedSideEffectVTAs.img),... % get linear index
                            aggregatedVTAsVoxelCoordinates(:,1),...
                            aggregatedVTAsVoxelCoordinates(:,2),...
                            aggregatedVTAsVoxelCoordinates(:,3));
                        aggregatedSideEffectVTAs.img(unique(linearIdx)) = ...
                            aggregatedSideEffectVTAs.img(unique(linearIdx)) + 1;
                    end

                    counterSideEffectVTAFull = counterSideEffectVTAFull + 1;
                end
            end
        end
    end
    fprintf('Counters %d and %d for %s.\n', counterSideEffectMinusEffectVTA, ...
        counterSideEffectVTAFull, sideEffectCategories(sideEffectIndex).name);
    switch sideEffectIndex
        case 1
            heatMapDysarthriaPooled = aggregatedSideEffectVTAs;
            [fhtmp, fhDysarthriaRight] = setupHeatMap();
            close(fhtmp)
            plotHeatMap(heatMapDysarthriaPooled, fhDysarthriaRight, 'right', 1, 'Dysarthria pooled',5);
        case 2
            heatMapDystoniaPooled = aggregatedSideEffectVTAs;
            [fhtmp, fhDystoniaRight] = setupHeatMap();
            close(fhtmp)
            plotHeatMap(heatMapDystoniaPooled, fhDystoniaRight, 'right', 1, 'Dystonia pooled',5);
        case 3
            heatMapParesthesiaPooled = aggregatedSideEffectVTAs;
            [fhtmp, fhParesthesiaRight] = setupHeatMap();
            close(fhtmp)
            plotHeatMap(heatMapParesthesiaPooled, fhParesthesiaRight, 'right', 1, 'Paresthesia pooled',5);
        case 4
            heatMapSpasmPooled = aggregatedSideEffectVTAs;
            [fhtmp, fhSpasmRight] = setupHeatMap();
            close(fhtmp)
            plotHeatMap(heatMapSpasmPooled, fhSpasmRight, 'right', 1, 'Spasm pooled',5);
    end
end
delete tmpVTAResliced.nii
heatMapDystoniaSpasmPooled = heatMapDystoniaPooled;
heatMapDystoniaSpasmPooled.img = heatMapDystoniaSpasmPooled.img + heatMapSpasmPooled.img;

%%
displayPrompt = true;
surfaceColors = 1/255*[...
215,25,28;...
253,174,97;...
255,255,191;...
166,217,106;...
26,150,65];
% fhAll = copyobj(fhMappingRight,0);
fhAll = setupHeatMapBilateral;
if displayPrompt; input('Press any key to continue'); end;

percentileThreshold = 95;
tmpThreshold = prctile((heatMapDysarthriaPooled.img(heatMapDysarthriaPooled.img>1)),...
    percentileThreshold);
surfaceDysarthria = plotHeatMapSurface(heatMapDysarthriaPooled, fhAll, 'right', tmpThreshold, '',surfaceColors(1,:));
if displayPrompt; input('Press any key to continue'); end;
tmpThreshold = prctile((heatMapDystoniaSpasmPooled.img(heatMapDystoniaSpasmPooled.img>1)),...
    percentileThreshold);
surfaceDystoniaSpasm = plotHeatMapSurface(heatMapDystoniaSpasmPooled, fhAll, 'right', tmpThreshold, '',surfaceColors(3,:));
if displayPrompt; input('Press any key to continue'); end;
tmpThreshold = prctile((heatMapParesthesiaPooled.img(heatMapParesthesiaPooled.img>1)),...
    percentileThreshold);
surfaceParesthesia = plotHeatMapSurface(heatMapParesthesiaPooled, fhAll, 'right', tmpThreshold, '',surfaceColors(2,:));
if displayPrompt; input('Press any key to continue'); end;

tmpThreshold = prctile((heatMapTremorReductionPooled.img(heatMapTremorReductionPooled.img>1)),...
    percentileThreshold);
surfaceTremor = plotHeatMapSurface(heatMapTremorReductionPooled, fhAll, 'right', tmpThreshold, '',surfaceColors(4,:));
legend([surfaceDysarthria surfaceDystoniaSpasm surfaceParesthesia surfaceTremor],...
    {'Dysarthria', 'Dystonia/ spasm', 'Paresthesia','Tremor reduction'},...
    'location','northeast')
%title('Superimposed "sweet spots", all 95th percentile')

if doPrint
    saveas(fhAll,strcat('..\..\Documents\Figures\','ClusterAllRev03c'))
    print(fhAll,strcat('..\..\Documents\Figures\','ClusterAllRev03c'),'-dtiffn','-r300')
end

%% Analyzing directional impact for DRTT DBS, see Steigerwald et al. (2016)
% for every patient two rows, first left lead, then right lead
% four columns for best level-best direction; best level-worst direction...
% worst level-best direction; worst level-worst direction
directionalTherapeuticWindow = ones(length(patientFolders)*2-8, 4);
% use levelTherapeuticWindow to normalize directionalTherapeutic Window
levelTherapeuticWindow = zeros(length(patientFolders)*2-8, 4);

for patientIndex = 1:7%length(patientFolders)
    % patients 215-218 not fully mapped
    % left lead
    if therapeuticWindows(patientIndex,9) >= ...
            therapeuticWindows(patientIndex,10) % comparing C234 vs C567
        levelTherapeuticWindow(2*patientIndex-1, 1:2) = ...
            therapeuticWindows(patientIndex,9);
        levelTherapeuticWindow(2*patientIndex-1, 3:4) = ...
            therapeuticWindows(patientIndex,10);
        directionalTherapeuticWindow(2*patientIndex-1,1:2) = ...
            [max(therapeuticWindows(patientIndex,2:4)) ...
            min(therapeuticWindows(patientIndex,2:4))];
        directionalTherapeuticWindow(2*patientIndex-1,3:4) = ...
            [max(therapeuticWindows(patientIndex,5:7)) ...
            min(therapeuticWindows(patientIndex,5:7))];
    else
        levelTherapeuticWindow(2*patientIndex-1, 1:2) = ...
            therapeuticWindows(patientIndex,10);
        levelTherapeuticWindow(2*patientIndex-1, 3:4) = ...
            therapeuticWindows(patientIndex,9);
        directionalTherapeuticWindow(2*patientIndex-1,1:2) = ...
            [max(therapeuticWindows(patientIndex,5:7)) ...
            min(therapeuticWindows(patientIndex,5:7))];
        directionalTherapeuticWindow(2*patientIndex-1,3:4) = ...
            [max(therapeuticWindows(patientIndex,2:4)) ...
            min(therapeuticWindows(patientIndex,2:4))];
    end

    % right lead
    if therapeuticWindows(patientIndex,19) >= ...
            therapeuticWindows(patientIndex,20) % comparing C101112 vs C131415
        levelTherapeuticWindow(2*patientIndex, 1:2) = ...
            therapeuticWindows(patientIndex,19);
        levelTherapeuticWindow(2*patientIndex, 3:4) = ...
            therapeuticWindows(patientIndex,20);
        directionalTherapeuticWindow(2*patientIndex,1:2) = ...
            [max(therapeuticWindows(patientIndex,12:14)) ...
            min(therapeuticWindows(patientIndex,12:14))];
        directionalTherapeuticWindow(2*patientIndex,3:4) = ...
            [max(therapeuticWindows(patientIndex,15:17)) ...
            min(therapeuticWindows(patientIndex,15:17))];
    else
        levelTherapeuticWindow(2*patientIndex, 1:2) = ...
            therapeuticWindows(patientIndex,20);
        levelTherapeuticWindow(2*patientIndex, 3:4) = ...
            therapeuticWindows(patientIndex,19);
        directionalTherapeuticWindow(2*patientIndex,1:2) = ...
            [max(therapeuticWindows(patientIndex,15:17)) ...
            min(therapeuticWindows(patientIndex,15:17))];
        directionalTherapeuticWindow(2*patientIndex,3:4) = ...
            [max(therapeuticWindows(patientIndex,12:14)) ...
            min(therapeuticWindows(patientIndex,12:14))];
    end
end

% levelTherapeuticWindow(levelTherapeuticWindow == 0) = 0.25; % to avoid division by zero
% levelTherapeuticWindow(1,1:2) = -0.25; % patient 201, left lead
% levelTherapeuticWindow(10,4) = -0.25; % patient 209

directionalTherapeuticWindowNormalized = directionalTherapeuticWindow - ...
        levelTherapeuticWindow;
% directionalTherapeuticWindowNormalized = directionalTherapeuticWindow ./ ...
%         levelTherapeuticWindow;
% directionalTherapeuticWindowNormalized(1,1:2) = -1*directionalTherapeuticWindowNormalized(1,1:2);
[p,tbl,stats] = anova1(directionalTherapeuticWindowNormalized);
multcompare(stats)

fhDirectionality = figure('color','w');
iosr.statistics.boxPlot(directionalTherapeuticWindowNormalized,'theme',...
    'colorboxes','showScatter',true,'medianColor',[1 0 0],...
    'scatterMarker','.','scatterColor',[0 0 0],'scatterSize',72);
hold on
plot([2.5 2.5],[-5 5],'k--','linewidth',1.5)
% plot([.5 4.5],[0 0],'k--','linewidth',0.8)
title('Directional stimulation DRTT DBS')
text(1,4,'most effective level');
text(3,4,'less effective level');
ylabel('Absolute change in therap. window to ring level')
xticklabels({'','','',''});
text(1,-5.5,'best direction','horizontalAlignment','center')
text(2,-5.5,'worst direction','horizontalAlignment','center')
text(3,-5.5,'best direction','horizontalAlignment','center')
text(4,-5.5,'worst direction','horizontalAlignment','center')
text(1.5,3,'*','FontSize',18,'horizontalAlignment','center')
text(3.5,3,'*','FontSize',18,'horizontalAlignment','center')
% Make median lines width 3.0
if doPrint
    saveas(fhDirectionality,strcat('..\..\Documents\Figures\','directionalityTherapeuticWindowsRev03'))
    print(fhDirectionality,strcat('..\..\Documents\Figures\','directionalityTherapeuticWindowsRev03'),'-dtiffn','-r300')
end

% text boxes for best level, less effective level; best directiona
% ylabel('Relative therapeutic window to ring level')

%% saving niftis
% tmpNifti = make_nii(heatMapTremorReductionPooled.img,...
% heatMapTremorReductionPooled.voxsize, ...
% mapWorldToVoxel([0 0 0],heatMapTremorReductionPooled));
% save_nii(tmpNifti,'heatMapTremorReductionPooledRev03.nii');
%
% tmpNifti = make_nii(heatMapDysarthriaPooled.img,...
% heatMapDysarthriaPooled.voxsize, ...
% mapWorldToVoxel([0 0 0],heatMapDysarthriaPooled));
% save_nii(tmpNifti,'heatMapDysarthriaPooledRev03.nii');
%
% tmpNifti = make_nii(heatMapDystoniaPooled.img,...
% heatMapDystoniaPooled.voxsize, ...
% mapWorldToVoxel([0 0 0],heatMapDystoniaPooled));
% save_nii(tmpNifti,'heatMapDystoniaPooledRev03.nii');
%
% tmpNifti = make_nii(heatMapSpasmPooled.img,...
% heatMapSpasmPooled.voxsize, ...
% mapWorldToVoxel([0 0 0],heatMapSpasmPooled));
% save_nii(tmpNifti,'heatMapSpasmPooledRev03.nii');
%
% tmpNifti = make_nii(heatMapDystoniaSpasmPooled.img,...
% heatMapDystoniaSpasmPooled.voxsize, ...
% mapWorldToVoxel([0 0 0],heatMapDystoniaSpasmPooled));
% save_nii(tmpNifti,'heatMapDystoniaSpasmPooledRev03.nii');
%
% tmpNifti = make_nii(heatMapParesthesiaPooled.img,...
% heatMapParesthesiaPooled.voxsize, ...
% mapWorldToVoxel([0 0 0],heatMapParesthesiaPooled));
% save_nii(tmpNifti,'heatMapParesthesiaPooledRev03.nii');

%% saving clusters, i.e. 95th percentile, as niftis
% percentileThreshold = 95;
% clusterTremorReduction = heatMapTremorReductionPooled;
% tmpThreshold = prctile((heatMapTremorReductionPooled.img(heatMapTremorReductionPooled.img>1)),...
%     percentileThreshold);
% clusterTremorReduction.img(clusterTremorReduction.img <= tmpThreshold) = 0;
% tmpNifti = make_nii(clusterTremorReduction.img,...
% clusterTremorReduction.voxsize, ...
% mapWorldToVoxel([0 0 0],clusterTremorReduction));
% save_nii(tmpNifti,'clusterTremorReductionRev03.nii');
%
% clusterDysarthria = heatMapDysarthriaPooled;
% tmpThreshold = prctile((heatMapDysarthriaPooled.img(heatMapDysarthriaPooled.img>1)),...
%     percentileThreshold);
% clusterDysarthria.img(clusterDysarthria.img <= tmpThreshold) = 0;
% tmpNifti = make_nii(clusterDysarthria.img,...
% clusterDysarthria.voxsize, ...
% mapWorldToVoxel([0 0 0],clusterDysarthria));
% save_nii(tmpNifti,'clusterDysarthriaRev03.nii');
%
% clusterDystoniaSpasm = heatMapDystoniaSpasmPooled;
% tmpThreshold = prctile((heatMapDystoniaSpasmPooled.img(heatMapDystoniaSpasmPooled.img>1)),...
%     percentileThreshold);
% clusterDystoniaSpasm.img(clusterDystoniaSpasm.img <= tmpThreshold) = 0;
% tmpNifti = make_nii(clusterDystoniaSpasm.img,...
% clusterDystoniaSpasm.voxsize, ...
% mapWorldToVoxel([0 0 0],clusterDystoniaSpasm));
% save_nii(tmpNifti,'clusterDystoniaSpasmRev03.nii');
%
% clusterParesthesia = heatMapParesthesiaPooled;
% tmpThreshold = prctile((heatMapParesthesiaPooled.img(heatMapParesthesiaPooled.img>1)),...
%     percentileThreshold);
% clusterParesthesia.img(clusterParesthesia.img <= tmpThreshold) = 0;
% tmpNifti = make_nii(clusterParesthesia.img,...
% clusterParesthesia.voxsize, ...
% mapWorldToVoxel([0 0 0],clusterParesthesia));
% save_nii(tmpNifti,'clusterParesthesiaRev03.nii');
