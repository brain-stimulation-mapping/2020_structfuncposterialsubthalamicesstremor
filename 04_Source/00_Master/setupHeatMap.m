function [figureHandleLeft, figureHandleRight] = setupHeatMap()
% Setup script for heat maps
% setupHeatMap()

fh1L = figure('color','w'); % heatmap left
fh1R = figure('color','w'); % heatmap right

% STN and RN of DISTAL atlas
fileAtlas = 'anatomyDISTAL.ply';
ptCloud = pcread(fileAtlas);
idxLeft = ptCloud.Location(:,1) < 0;
idxRight = ptCloud.Location(:,1) >= 0;
pcColors = unique(ptCloud.Color,'rows');
colorIdx = [14 13]; % color code for RN and STN in DISTAL atlas
% identifying the index of points with that color
% tmpIdx is a logical index 0 if R, G, or B part is equal to STN or RN

% STN
tmpIdx = ptCloud.Color == pcColors(colorIdx(2),:); 
idxSTN = sum(tmpIdx,2) == 3;
[STNLeft,~] = boundary(double(ptCloud.Location((idxSTN & idxLeft),1)),...
    double(ptCloud.Location((idxSTN & idxLeft),2)),...
    double(ptCloud.Location((idxSTN & idxLeft),3)),0.25);
[STNRight,~] = boundary(double(ptCloud.Location((idxSTN & idxRight),1)),...
    double(ptCloud.Location((idxSTN & idxRight),2)),...
    double(ptCloud.Location((idxSTN & idxRight),3)),0.25);
% RN
tmpIdx = ptCloud.Color == pcColors(colorIdx(1),:); 
idxRN = sum(tmpIdx,2) == 3;
[RNLeft,~] = boundary(double(ptCloud.Location((idxRN & idxLeft),1)),...
    double(ptCloud.Location((idxRN & idxLeft),2)),...
    double(ptCloud.Location((idxRN & idxLeft),3)),0.25);
[RNRight,~] = boundary(double(ptCloud.Location((idxRN & idxRight),1)),...
    double(ptCloud.Location((idxRN & idxRight),2)),...
    double(ptCloud.Location((idxRN & idxRight),3)),0.25);

figure(fh1L)
hold off
trisurf(STNLeft,ptCloud.Location((idxSTN & idxLeft),1),...
    ptCloud.Location((idxSTN & idxLeft),2),...
    ptCloud.Location((idxSTN & idxLeft),3),...
    'FaceColor',[.25 .25 .25],'FaceAlpha',0.25,... % alternative FaceColor pcColors(colorIdx(1),:)
    'LineStyle','none','EdgeColor',[.25 .25 .25])
hold on
trisurf(RNLeft,ptCloud.Location((idxRN & idxLeft),1),...
    ptCloud.Location((idxRN & idxLeft),2),...
    ptCloud.Location((idxRN & idxLeft),3),...
    'Facecolor',[.4 .4 .4],'FaceAlpha',0.25,...
    'LineStyle','none','EdgeColor',[.4 .4 .4])
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
xlim([-20 0]); ylim([-25 -5]); zlim([-20 0]);

figure(fh1R)
hold off
trisurf(STNRight,ptCloud.Location((idxSTN & idxRight),1),...
    ptCloud.Location((idxSTN & idxRight),2),...
    ptCloud.Location((idxSTN & idxRight),3),...
    'Facecolor',[.25 .25 .25],'FaceAlpha',0.25,...
    'LineStyle','none','EdgeColor',[.25 .25 .25])
hold on
trisurf(RNRight,ptCloud.Location((idxRN & idxRight),1),...
    ptCloud.Location((idxRN & idxRight),2),...
    ptCloud.Location((idxRN & idxRight),3),...
    'Facecolor',[.4 .4 .4],'FaceAlpha',0.25,...
    'LineStyle','none','EdgeColor',[.4 .4 .4])
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
xlim([0 20]); ylim([-25 -5]); zlim([-20 0]);

figureHandleLeft = fh1L;
figureHandleRight = fh1R;
end
