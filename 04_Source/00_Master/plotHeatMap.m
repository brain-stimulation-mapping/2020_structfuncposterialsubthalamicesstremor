function [] = plotHeatMap(heatMap, figureHandle, hemisphere, threshold, figureTitle, downSampleFactor)
if nargin < 6
    downSampleFactor = 5;
end

tmpIdx = find(heatMap.img > threshold);
if ~isempty(tmpIdx)
    [xx,yy,zz] = ind2sub(size(heatMap.img),tmpIdx);
    heatMapVoxelCoordinates = [xx,yy,zz];
    heatMapWorldCoordinates = mapVoxelToWorld(heatMapVoxelCoordinates,heatMap);
    
%     downSampleFactor = 5;
    markerSize = 6;
    figure(figureHandle)
    h = scatter3(heatMapWorldCoordinates(1:downSampleFactor:end,1),...
        heatMapWorldCoordinates(1:downSampleFactor:end,2),...
        heatMapWorldCoordinates(1:downSampleFactor:end,3),...
        markerSize,heatMap.img(tmpIdx(1:downSampleFactor:end)),'filled');
    h.MarkerFaceAlpha = 0.4;
    
    if strcmp(hemisphere,'left')
        view(-30,30)
    elseif strcmp(hemisphere,'right')
        view(30,30)
    else
        view(0,30)
    end
    cb = colorbar;
    if max(heatMap.img(:)) > threshold+1
        caxis([threshold+1 max(heatMap.img(:))])
    else
        caxis([threshold threshold+1])
    end
    cb.Label.String = 'Number of activation';
end
title(figureTitle)

end
